import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

export default {
    getCocktailByName(name) {
        return Vue.http.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${name}`)
    },

    getCocktailByIngredient(ingredient) {
        return Vue.http.get(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${ingredient}`)
    },

    getById(id) {
        return Vue.http.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`)
    },

    surprise() {
        return Vue.http.get('https://www.thecocktaildb.com/api/json/v1/1/random.php');
    }
}