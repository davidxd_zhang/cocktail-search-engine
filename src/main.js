import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import './assets/app.scss'

import VueRouter from 'vue-router';

import Home from './components/Home.vue'
import CocktailShow from './components/CocktailShow.vue'


Vue.config.productionTip = false

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Home },
  { path: '/cocktails/:drinkId', component: CocktailShow}
];

const router = new VueRouter({
  routes,
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
