# Cocktail Search Engine
A simple application to search a cocktail using a provided API

## Installation
* npm install
* npm run serve

## Behind work
* Started with a handmade mockup. If have more time, a mokeup from Figma will be better.
* The main feature of this application is able to make a research and able to show it.
* Started with a input with a search button and send a request with this key word.
* Rreturned results need able be shown on the page.
* Made a list view to show all the results.
* When clicking on a single item, it bring you to another page with a route.
* Passing id in url to able to keep the result after the page is refreshed. If passed in a prop, value in prop will be empty after a refresh.
* UI and UX ideas are refrence to https://www.thecocktaildb.com/

## What can be done for next update
* Implement Sentry, it helps to catch all front-end and back-end errors.
* Add some unit tests.
* Implement Jenkin for automated CI/CD
* Add some fun feature, like able to add to your favorit.(Then it need a database)
* Have a better UI/UX. (This time, concentrated more on the functionalities)


